/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #3: Smart Dam
   Author:
   - Andrea Casadei 800898
*/

#ifndef LED_H
#define LED_H

#include "Light.h"

/**
 * Implements a Led as a Light source.
 */
class Led : public Light {
  public:
    Led(unsigned pin);
    void switchOn() override;
    void switchOff() override;
    bool isTurnedOn() override;
  private:
    unsigned pin;
    bool isOn;
};

#endif
