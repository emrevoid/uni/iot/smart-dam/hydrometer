/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #3: Smart Dam
   Author:
   - Andrea Casadei 800898
*/

#ifndef TASK_H
#define TASK_H

class Task{
  public:
    Task(){
        active = false;
    }

    /* periodic */
    virtual void init(int period){
        myPeriod = period;
        periodic = true;  
        active = true;
        timeElapsed = 0;
    }

    /* aperiodic */
    virtual void init(){
        timeElapsed = 0;
        periodic = false;
        active = true;
        completed = false;
    }

    virtual void tick() = 0;

    bool updateAndCheckTime(int basePeriod){
        timeElapsed += basePeriod;
        if (timeElapsed >= myPeriod){
            timeElapsed = 0;
            return true;
        } else {
            return false; 
        }
    }

    int getPeriod(){
        return myPeriod;
    }

    void setCompleted(){
        completed = true;
        active = false;
    }

    bool isCompleted(){
        return completed;
    }

    bool isPeriodic(){
        return periodic;
    }

    bool isActive(){
        return active;
    }

    virtual void setActive(bool active){
        timeElapsed = 0;
        this->active = active;
    }

    float getDistance(){
        return distance;
    }

    void setDistance(float distance) {
        this->distance = distance;
    }
    
  private:
    int myPeriod;
    int timeElapsed;
    bool active;
    bool periodic;
    bool completed;
    float distance = 1.0;
};

#endif