/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #3: Smart Dam
   Author:
   - Andrea Casadei 800898
*/

#include "UltrasonicSensor.h"
#include <Arduino.h>

UltrasonicSensor::UltrasonicSensor(unsigned triggerPin,
                                    unsigned echoPin)
{
    pinMode(triggerPin, OUTPUT);
    pinMode(echoPin, INPUT);
    this->triggerPin = triggerPin;
    this->echoPin = echoPin;
}

double
UltrasonicSensor::read()
{
    // send pulse
    digitalWrite(triggerPin, LOW);
    delayMicroseconds(firstDelay);
    digitalWrite(triggerPin, HIGH);
    delayMicroseconds(secondDelay);
    digitalWrite(triggerPin, LOW);

    // receive echo
    float timeMicroseconds = pulseIn(echoPin, HIGH);
    float time = timeMicroseconds / 1000.0 / 1000.0 / 2;
    float distance = time * soundSpeed;

    return distance;
}

