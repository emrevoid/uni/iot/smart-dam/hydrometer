/*
  Sistemi Embedded e IoT 2020-2021
  Assignment #3: Smart Dam
  Author:
  - Andrea Casadei 800898
*/
#define WM_ASYNC

#include <Arduino.h>
#include <ESP8266HTTPClient.h>
#include <WiFi.h>

#include "Led.h"
#include "UltrasonicSensor.h"

// PIN REFERENCE
// https://components101.com/sites/default/files/component_pin/NodeMCU-ESP8266-Pinout.jpg

#define PIN_LED 5
#define PIN_TRIG 13
#define PIN_ECHO 12

// push frequencies
#define F1 10000
#define F2 5000

// distance
#define D1 0.50 // 100
#define D2 0.20 // 40
#define deltaD 4

enum { NORMAL, PRE_ALARM, ALARM } state;

Led* led;
UltrasonicSensor* sonar;

void setup() {
  Serial.begin(115200);

}

void loop() {
  {
  // hydrometer->update();
  // float d = sonar->read();
  // int c = 0;

  // if (d > D2 && d < D1){
  //   // PRE-ALARM
  //   led->switchOff();
  //   while(c<F1){
  //     led->switchOn();
  //     delay(1000);
  //     c++;
  //     led->switchOff();
  //     delay(1000);
  //     c++;
  //   }
  // } else {
  //   // ALARM
  //   led->switchOn();
  //   delay(F2*1000);
  // }

  // ThingPropertyValue readDistance = {.number = d};
  // sonarValue.setValue(readDistance);

  // if (WiFi.status() != WL_CONNECTED) {
  //   wifiManager.autoConnect("IOT-Dam", "IOT-Dam");
  // }
  }


}

